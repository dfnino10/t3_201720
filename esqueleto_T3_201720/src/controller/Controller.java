package controller;

import java.io.File;

import model.exceptions.TripNotFoundException;
import model.logic.STSManager;
import api.ISTSManager;

public class Controller {
	
	private static String STOPS="./data/stops.txt";
	/**
	 * Reference to the routes and stops manager
	 */
	private static ISTSManager  manager = new STSManager();

	public static void loadStops() {
		manager.loadStops(STOPS);		
	}

	public static void readBusUpdates() throws Exception{
		File f = new File("data");
		File[] updateFiles = f.listFiles();
		for (int i = 0; i < updateFiles.length; i++) {
			manager.readBusUpdate(updateFiles[i]);
		}
	}
	
	public static void listStops(Integer tripId) throws TripNotFoundException{
		throw new TripNotFoundException();
	}
	

}
