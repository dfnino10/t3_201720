package api;

import java.io.File;

import model.data_structures.DoubleLinkedList;
import model.data_structures.IList;
import model.vo.BusUpdateVO;
import model.vo.VOStop;
import model.vo.VOTrip;

public interface ISTSManager {

	
	/**
	 * Reads a file in json format containing bus updates in real time
	 * @param rtFile - The file to read
	 */
	public void readBusUpdate(File rtFile) throws Exception;
	
	public IList<String> listStops (Integer tripID) throws Exception;

	public void loadStops(String archivo);
}
