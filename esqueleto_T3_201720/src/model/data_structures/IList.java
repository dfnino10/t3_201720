package model.data_structures;


/**
 * Abstract Data Type for a list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface IList<T> extends Iterable<T> {

	Integer getSize();

	public T getFirst();
	public T getLast();
	public void addFirst(T elemento);
	public void addLast(T elemento);
	public void addPos(T elemento, int pos) throws Exception;
	public T darElementoPos(int pos) throws Exception;
	public T removePos(int pos) throws Exception;
	public boolean isEmpty();
}
