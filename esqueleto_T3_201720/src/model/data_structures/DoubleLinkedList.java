package model.data_structures;

import java.util.Iterator;

import javax.lang.model.element.Element;

public class DoubleLinkedList<T> implements IList<T>, Iterable<T>{

	public static class Node <T>
	{
		private T element;
		private Node<T> next;
		private Node<T> prev;

		public Node(T e)
		{
			element= e;
			next=null;
			prev=null;
		}

		public Node<T> darSiguiente()
		{
			return this.next;
		}
		public Node<T> darAnterior()
		{
			return this.prev;
		}
		
		public T darElemento()
		{
			return this.element;
		}
	}

	private Integer size;
	private Node<T> first;
	private Node<T> last;

	public DoubleLinkedList() 
	{
		size=0;
		first=null;
		last=null;
	}

	@Override
	public Iterator<T> iterator() {
		return new Iterator<T>() 
		{
			private Node<T> current= first;

			@Override
			public boolean hasNext() 
			{
				// TODO Auto-generated method stub
				return current!=null;
			}

			@Override
			public T next() 
			{
				// TODO Auto-generated method stub
				Node<T> result = null;
		        if(current != null) {
		          result = (Node<T>) current;
		          current = current.darSiguiente();
		        }
		        return (T) result;
			}
			
		};
	}

	@Override
	public Integer getSize() {

		return size;
	}


	public T getFirst() {

		return first.element;
	}


	public T getLast() {

		return last.element;
	}

	public void addFirst ( T elemento)
	{
		Node<T> nodo= new Node(elemento);
		if(first==null)
		{
			first=nodo;
			last=nodo;
		}
		else
		{
			nodo.next=first;
			first.prev=nodo;
			first=nodo;
			nodo.prev=null;
		}
		size++;
	}

	public void addLast(T elemento)
	{
		Node<T> nodo= new Node(elemento);
		if(first==null)
		{
			addFirst(elemento);
		}
		else
		{
			last.next=nodo;
			nodo.prev=last;
			last=nodo;
			nodo.next=null;
			
			size++;
		}
	}

	public void addPos(T elemento, int pos) throws Exception
	{
		Node<T> nodo= new Node(elemento);
		if (pos>=size)
		{
			throw new Exception("La posicion se sale de la lista");
		}
		else if(first== null||pos==0)
		{
			addFirst(elemento);
		}
		else if (pos==size-1)
		{
			addLast(elemento);
		}
		else
		{
			int i=0;
			Iterator it = iterator();
			while(it.hasNext())
			{
				Node<T> n = (Node<T>) it.next();
				Node<T> p = n.prev;
				if (i==pos)
				{
					nodo.next=n;
					nodo.prev=n.prev;
					p.next=nodo;
					n.prev=nodo;
					break;
				}
				i++;
			}
			size++;
		}
	}

	public T darElementoPos(int pos) throws Exception
	{
		if(pos>=size)
		{
			throw new Exception("La posicion se sale de la lista");
		}
		else
		{
			int i=0;
			T elemento= null;
			Iterator it = iterator();
			while(it.hasNext())
			{
				Node<T> n = (Node<T>) it.next();
				if (i==pos)
				{
					elemento=n.darElemento();
					break;
				}	
				i++;
			}
			return elemento;
		}
	}
	public T removePos(int pos) throws Exception
	{
		Node <T> nodoR = null;
		if(pos>=size)
		{
			throw new Exception("La posici�n se sale de la lista.");
		}
		else
		{
			int i=0;
			Iterator it = iterator();
			while(it.hasNext())
			{
				Node<T> nodo= (Node<T>) it.next();
				if (i==pos)
				{
					if(size==1)
					{
						nodoR=first;
						first=null;
						last=null;
					}
					else if(pos==0)
					{
						nodoR=first;
						first=first.next;
						first.prev=null;
					}
					else if(pos==size-1)
					{
						nodoR=last;
						last=last.prev;
						last.next=null;
					}
					else
					{
						nodoR=nodo;
						Node<T>n=nodo.next;
						nodo.prev.next=n;
						n.prev=nodo.prev;
					}
				}
				i++;
			}
			size--;
		}
		return nodoR.element;		
	}
	
	public boolean isEmpty()
	{
		return size==0?true:false;
	}
	
	public boolean repeatedElement(T element)
	{
		boolean si=false;
		
		if(isEmpty())
		{
			return si;
		}
		else 
		{
			Iterator it = iterator();
			while(it.hasNext())
			{
				Node<T> nodo= (Node<T>) it.next();
				T e1= nodo.darElemento();
				
				String className=element.getClass().getSimpleName();
				
				
				if(className.equals(int.class.getSimpleName())||className.equals(double.class.getSimpleName())||className.equals(float.class.getSimpleName())||className.equals(short.class.getSimpleName()))
				{
					if((double)element==(double)e1)
					{
						si=true;
					}
				}
				else if(element.equals(e1) )
				{
					si=true;
				}
			}
		}
		
		return si;
	}
}

