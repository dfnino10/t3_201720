package model.vo;

public class BusUpdateVO 
{
	private String vehicleNo;
	private String tripId;
	private String routeNo;
	private String direction;
	private String destination;
	private String pattern;
	private String latitude;
	private String longitude;
	private String recordedTime;
	private String routeMap;
	
	public BusUpdateVO(String vN, String tI, String rN, String dir, String dest, String patt, String lati, String longi, String rT, String rM)
	{
		vehicleNo=vN;
		tripId=tI;
		routeNo=rN;
		direction=dir;
		destination=dest;
		pattern=patt;
		latitude=lati;
		longitude=longi;
		recordedTime=rT;
		routeMap=rM;
	}
	
	public String getVehicleNo(){return vehicleNo;}
	public String getTripId(){return tripId;}
	public String getRouteNo(){return routeNo;}
	public String getDirection(){return direction;}
	public String getDestination(){return destination;}
	public String getPattern(){return pattern;}
	public String getLatitutde(){return latitude;}
	public String getLongitude(){return longitude;}
	public String getRecordedTime(){return recordedTime;}
	public String getRouteMap(){return routeMap;}
	
}
