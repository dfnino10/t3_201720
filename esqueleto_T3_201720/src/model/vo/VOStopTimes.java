package model.vo;

public class VOStopTimes 
{
	private String tripId;
	private String arrivalTime;
	private String departureTime;
	private String stopId;
	private String stopSequence;
	private String stopHeadSign;
	private String pickupType;
	private String dropOffType;
	private String shapeDistTraveled;
	
	public VOStopTimes(String trip_id,String arrival_time,String departure_time,String stop_id,String stop_sequence,String stop_headsign,String pickup_type,String drop_off_type,String shape_dist_traveled)
	{
		tripId=trip_id;
		arrivalTime=arrival_time;
		departureTime=departure_time;
		stopId=stop_id;
		stopSequence=stop_sequence;
		stopHeadSign=stop_headsign;
		pickupType=pickup_type;
		dropOffType=drop_off_type;
		shapeDistTraveled=shape_dist_traveled;
	}
	
	public int getTripId()
	{
		return Integer.parseInt(tripId);
	}
	
	public String getArrivalTime()
	{
		return arrivalTime;
	}
	
	public String getDepartureTime()
	{
		return departureTime;
	}
	
	public String getStopId()
	{
		return stopId;
	}
	
	public String getStopSequence()
	{
		return stopSequence;
	}
	
	public String getStopHeadSign()
	{
		return stopHeadSign;
	}
	
	public String  getPickupType()
	{
		return pickupType;
	}
	
	public String getDropOffType()
	{
		return dropOffType;
	}
	
	public String getShapeDistTraveled()
	{
		return shapeDistTraveled;
	}
}
