package model.vo;

import model.data_structures.DoubleLinkedList;

/**
 * Representation of a Stop object
 */
public class VOStop
{
	private String stopId, stopCode, stopName, stopDesc, stopLat, stopLon, zoneId, stopUrl, locationType, parentStation;
	
	DoubleLinkedList<VOStopTimes> listaStopTimes;
	
	public VOStop(String stop_id,String stop_code,String stop_name,String stop_desc, String stop_lat, String stop_lon, String zone_id, String stop_url,String location_type,String parent_station)
	{
		stopId=stop_id;
		stopCode=stop_code;
		stopName=stop_name;
		stopDesc=stop_desc;
		stopLat=stop_lat;
		stopLon=stop_lon;
		zoneId=zone_id;
		stopUrl=stop_url;
		locationType=location_type;
		parentStation=parent_station;
		listaStopTimes=new DoubleLinkedList<VOStopTimes>();
	}
	/**
	 * @return id - stop's id
	 */
	public int getStopId() 
	{
		// TODO Auto-generated method stub
		return Integer.parseInt(stopId);
	}
	public DoubleLinkedList<VOStopTimes> getListaStopTimes() 
	{
		return listaStopTimes;
	}

	/**
	 * @return name - stop name
	 */
	public String getStopName() 
	{
		// TODO Auto-generated method stub
		return stopName;
	}
	
	public String getStopCode()
	{
		return stopCode;
	}
	public String getStopDesc()
	{
		return stopDesc;
	}
	public String getStopLat()
	{
		return stopLat;
	}
	public String getStopLon()
	{
		return stopLon;
	}
	public String getZoneId()
	{
		return zoneId;
	}
	public String getStopUrl()
	{
		return stopUrl;
	}
	public String getLocationType()
	{
		return locationType;
	}
	public String getParentStation()
	{
		return parentStation;
	}


}

