package model.vo;

import model.data_structures.DoubleLinkedList;
import model.data_structures.QueueList;

public class VOTrip{

	private String routeId, serviceId, tripId, tripHeadSign, tripShortName, directionId, blockId, shapeId, wheelchairAccessible, bikesAllowed;

	DoubleLinkedList<VOStop> stops;

	public VOTrip(String route_id, String service_id, String trip_id, String trip_headsign, String trip_short_name, String direction_id, String block_id, String shape_id, String wheelchair_accessible, String bikes_allowed)
	{
		routeId=route_id;
		serviceId=service_id;
		tripId=trip_id;
		tripHeadSign=trip_headsign;
		tripShortName=trip_short_name;
		directionId=direction_id;
		blockId=block_id;
		shapeId=shape_id;
		wheelchairAccessible=wheelchair_accessible;
		bikesAllowed=bikes_allowed;
		stops = new DoubleLinkedList<VOStop>();
	}

	public String getRouteId(){return routeId;}
	public String getServiceId(){return serviceId;}
	public String getTripId(){return tripId;}
	public String getTripHeadSign(){return tripHeadSign;}
	public String getTripShortName(){return tripShortName;}
	public String getDirectionId(){return directionId;}
	public String getBlockId(){return blockId;}
	public String getShapeId(){return shapeId;}
	public String getWheelChairAccessible(){return wheelchairAccessible;}
	public String getBikesAllowed(){return bikesAllowed;}
	public DoubleLinkedList<VOStop> getStops(){return stops;}
}
