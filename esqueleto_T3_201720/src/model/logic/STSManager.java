package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import javax.swing.text.html.HTMLDocument.Iterator;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import model.data_structures.DoubleLinkedList;
import model.data_structures.IList;
import model.data_structures.QueueList;
import model.exceptions.TripNotFoundException;
import model.vo.BusUpdateVO;
import model.vo.VOStop;
import model.vo.VOStopTimes;
import model.vo.VOTrip;
import model.vo.VORoute;
import api.ISTSManager;

public class STSManager implements ISTSManager{

	private static String STOPS="./data/stops.txt";
	private static String STOP_TIMES="./data/stop_times.txt";
	private static String ROUTES="./data/routes.txt";
	private static String TRIPS="./data/trips.txt";
	private static String BUS_UPDATES="./data/BUSES_SERVICE_";

	private DoubleLinkedList<VORoute> listaRoutes;
	private DoubleLinkedList<VOStop> listaStops;

	public STSManager() 
	{
		// TODO Auto-generated constructor stub
		loadRoutes(ROUTES);
	} 

	public void loadRoutes(String rOUTES2) 
	{
		try 
		{
			loadStops(STOPS);
			listaRoutes= new DoubleLinkedList<VORoute>();
			FileReader reader = new FileReader(rOUTES2);
			BufferedReader br = new BufferedReader(reader);
			String linea=br.readLine();
			while((linea = br.readLine())!=null)
			{
				String[] info=linea.split(",");
				VORoute elemento= new VORoute(info[0], info[1], info[2], info[3], info[4], info[5], info[6], info[7], info[8]);
				listaRoutes.addLast(elemento);				
			}
			br.close();

			loadTrips(TRIPS, listaRoutes);
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		}
		catch (IOException e1)
		{
			e1.printStackTrace();
		}

	}

	public void loadTrips(String tRIPS2, DoubleLinkedList<VORoute> listaRutas) {
		// TODO Auto-generated method stub
		FileReader reader;
		try 
		{
			reader = new FileReader(tRIPS2);
			BufferedReader br = new BufferedReader(reader);
			String linea=br.readLine();

			while(br.readLine()!=null)
			{
				String[] split=linea.split(",");
				VOTrip elemento = new VOTrip(split[0], split[1], split[2], split[3], split[4], split[5], split[6], split[7], split[8], split[9]);

				for(int i=0; i<listaRutas.getSize();i++)
				{
					VORoute ruta= listaRutas.darElementoPos(i);
					if(ruta.getRouteId()==Integer.parseInt(split[0]))
					{
						ruta.getlistaTrips().addLast(elemento);
						guardarStops(ruta.getlistaTrips());
					}
				}

			}
			br.close();

		} 
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	@Override
	public void loadStops(String sTOPS2) 
	{
		// TODO Auto-generated method stub
		FileReader reader;
		try 
		{
			listaStops= new DoubleLinkedList<VOStop>();
			reader = new FileReader(sTOPS2);
			BufferedReader br = new BufferedReader(reader);
			String linea=br.readLine();

			while(br.readLine()!=null)
			{
				String[] split=linea.split(",");
				VOStop elemento = new VOStop(split[0], split[1], split[2], split[3], split[4], split[5], split[6], split[7], split[8], split[9]);
				listaStops.addFirst(elemento);
			}
			br.close();
			loadStopTimes(STOP_TIMES, listaStops);
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public void guardarStops(DoubleLinkedList<VOTrip> listaTrip)
	{
		try
		{
			for (int i =0 ; i<listaTrip.getSize();i++)
			{
				VOTrip trip = listaTrip.darElementoPos(i);

				for(int k=0; k<listaStops.getSize(); k++)
				{
					VOStop stop= listaStops.darElementoPos(k);

					for(int j =0; j<stop.getListaStopTimes().getSize();j++)
					{
						VOStopTimes stopTime= stop.getListaStopTimes().darElementoPos(j);
						if(stopTime.getTripId()==Integer.parseInt(trip.getTripId()))
						{
							trip.getStops().addLast(stop);
						}

					}

				}
			}
		}
		catch(Exception e)
		{

		}
	}


	public void loadStopTimes(String stopTimesFile, DoubleLinkedList<VOStop> lista) 
	{
		// TODO Auto-generated method stub
		FileReader reader;
		try 
		{
			reader = new FileReader(stopTimesFile);
			BufferedReader br = new BufferedReader(reader);
			String linea=br.readLine();

			while(br.readLine()!=null)
			{
				String[] split=linea.split(",");
				VOStopTimes elemento = new  VOStopTimes(split[0], split[1], split[2], split[3], split[4], split[5], split[6], split[7], split[8]);

				for (int i =0; i<lista.getSize();i++)
				{
					VOStop stop = lista.darElementoPos(i);
					if(stop.getStopId()==Integer.parseInt(elemento.getStopId()))
					{
						stop.getListaStopTimes().addLast(elemento);
					}
				}
			}
			br.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void readBusUpdate(File rtFile) throws Exception
	{
		Gson gson = new Gson();
		BufferedReader br = null;
		try
		{
			br= new BufferedReader(new FileReader(rtFile));
			BusUpdateVO[] busU=gson.fromJson(br, BusUpdateVO[].class);

			for (int i=0; i<busU.length;i++)
			{
				BusUpdateVO bus = busU[i];
				String routeN=bus.getRouteNo();				
				for(int j=0;j<listaRoutes.getSize();j++)
				{
					VORoute ruta = listaRoutes.darElementoPos(j);
					if(routeN.equals(ruta.getShortName()))
					{
						ruta.getlistaBus().addLast(bus);
					}
				}
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				br.close();
			}
			catch(IOException e1)
			{
				e1.printStackTrace();
			}			
		}
	}

	public double getDistance(double lat1, double lon1, double lat2, double lon2) {
		// TODO Auto-generated method stub
		final int R = 6371*1000; // Radious of the earth
		double latDistance = toRad(lat2-lat1);
		double lonDistance = toRad(lon2-lon1);
		double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) +Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) *Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		double distance = R * c;
		return distance;
	}
	
	 private double toRad(double value) {return value * Math.PI / 180;}
	 
	 
	@Override
	public IList<String> listStops(Integer tripID) throws Exception {
		// TODO Auto-generated method stub
		IList aRetornar=new DoubleLinkedList<String>();
		for (int i = 0; i < listaRoutes.getSize(); i++) 
		{
			DoubleLinkedList<VOStop> listaStops= null; 
			VOTrip trip=null;
			VORoute ruta = listaRoutes.darElementoPos(i);
			for (int j = 0; j < ruta.getlistaTrips().getSize(); j++)
			{
				trip = ruta.getlistaTrips().darElementoPos(i);
				if(tripID== Integer.parseInt(trip.getTripId()))
				{
					listaStops= trip.getStops();
					break;
				}
			}
			if(listaStops.isEmpty())
				throw new TripNotFoundException();
			DoubleLinkedList<BusUpdateVO>listaBus=ruta.getlistaBus();
			for (int j = 0; j < listaBus.getSize(); j++) 
			{
				BusUpdateVO bus= listaBus.darElementoPos(j);
				if(bus.getTripId().equals(trip.getTripId()))
				{
					for (int k = 0; k < listaStops.getSize(); k++) 
					{
						VOStop stop = listaStops.darElementoPos(k);
						if(getDistance(Double.parseDouble(bus.getLatitutde()), Double.parseDouble(bus.getLongitude()), Double.parseDouble(stop.getStopLat()), Double.parseDouble(stop.getStopLon()))<=70)
						{
							aRetornar.addLast(stop.getStopName());
						}
					}					
				}
			}
		}
		return aRetornar;
	}


}
