package esqueleto_T3_201720.test;

import junit.framework.TestCase;
import model.data_structures.QueueList;

public class QueueListTest extends TestCase 
{
	private QueueList queueList;

	public void setupEscenario1( )
	{
		queueList= new QueueList<>();          
	}

	public void testAddFirst()
	{
		setupEscenario1();

		int a=0;
		int b=3;
		int c=5;

		queueList.addFirst(a);

		assert queueList.getSize()!=0:"La lista deber�a tener elementos.";

		queueList.addFirst(b);
		queueList.addFirst(c);


		assert queueList.getSize()==3:"La lista deber�a tener 3 elementos.";
		assert (int)queueList.getLast()==a:"El primer elemento es incorrecto.";
	}

	public void setupEscenario2( )
	{
		queueList= new QueueList<>();

		int a=0;
		int b=3;
		int c=5;

		queueList.addFirst(a);
		queueList.addFirst(b);
		queueList.addFirst(c);
	}

	public void testGetLast()
	{
		setupEscenario1();
		try
		{
			assert queueList.getLast()==null :"La lista est� vac�a";
		}
		catch(NullPointerException e)
		{
			setupEscenario2();

			assert (int)queueList.getLast()==0 :"El �ltimo elemento de la lista es incorrecto";
		}
	}		


	public void testGetSize()
	{
		setupEscenario1();
		assert queueList.getSize()==0 :"El tama�o de la lista es incorrecto";

		setupEscenario2();

		assert queueList.getSize()==3 :"El tama�o de la lista es incorrecto";
	}
}
