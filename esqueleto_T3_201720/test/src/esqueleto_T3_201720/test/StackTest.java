package esqueleto_T3_201720.test;

import junit.framework.TestCase;
import model.data_structures.Stack;

public class StackTest extends TestCase
{
	private Stack stackList;

	public void setupEscenario1( )
	{
		stackList= new Stack<>();          
	}

	public void testAddFirst()
	{
		setupEscenario1();

		int a=0;
		int b=3;
		int c=5;

		stackList.addFirst(a);

		assert stackList.getSize()!=0:"La lista deber�a tener elementos.";

		stackList.addFirst(b);
		stackList.addFirst(c);


		assert stackList.getSize()==3:"La lista deber�a tener 3 elementos.";
		assert (int)stackList.getFirst()==c:"El primer elemento es incorrecto.";
	}

	public void setupEscenario2( )
	{
		stackList= new Stack<>();

		int a=0;
		int b=3;
		int c=5;

		stackList.addFirst(a);
		stackList.addFirst(b);
		stackList.addFirst(c);
	}

	public void testGetFirst()
	{
		setupEscenario1();
		try
		{
			assert stackList.getFirst()==null :"La lista est� vac�a";
		}
		catch(NullPointerException e)
		{
			setupEscenario2();

			assert (int)stackList.getFirst()==5 :"El primer elemento de la lista es incorrecto";
		}
	}		


	public void testGetSize()
	{
		setupEscenario1();
		assert stackList.getSize()==0 :"El tama�o de la lista es incorrecto";

		setupEscenario2();

		assert stackList.getSize()==3 :"El tama�o de la lista es incorrecto";
	}
}
